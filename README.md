# Simple Todo application -- server

## Local development

Running the app locally can be done as follows:

1. Create a file named `.env` in the project root, and copy the contents from `.env.exmaple` into it. This command should work: `cp .env.example .env`
2. Spin up the database using `docker compose up -d`
3. Install dependencies with `npm i`
4. Generate prisma files with `npm run prisma`
5. Run migrations with `npm run migrate`
6. Run the server in watch mode using `npm run dev`

## Building

Building the server can be done with the `npm run build` command. The built app
will be inside the `dist/` folder. You can run the built app with `npm run start`.
