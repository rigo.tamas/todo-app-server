/*
  Warnings:

  - You are about to drop the column `refresTokenVersion` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "refresTokenVersion",
ADD COLUMN     "refreshTokenVersion" INTEGER NOT NULL DEFAULT 0;
