import { PrismaClient } from '@prisma/client';
import { EnvConfig } from './config/config';
import { fastifyServerFactory } from './fastify/fastify';
import { graphqlContextFactory } from './graphql/resolvers';

export const app = async ({ config }: { config: EnvConfig }) => {
  const prisma = new PrismaClient();
  const graphqlContext = graphqlContextFactory({ config, prisma });
  const fastifyServer = await fastifyServerFactory({ graphqlContext, config, prisma });
  fastifyServer.listen({ port: parseInt(config.PORT) });
};
