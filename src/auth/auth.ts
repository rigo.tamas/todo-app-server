import { FastifyReply, FastifyRequest } from 'fastify';
import { sign, verify } from 'jsonwebtoken';

type TokenPayload = { userId: number };
export type RefreshTokenPayload = TokenPayload & { refreshTokenVersion: number };

export const createAccessToken = ({ userId, secret }: { userId: number; secret: string }) => {
  return sign({ userId }, secret, { expiresIn: '5m' });
};

export const createRefreshToken = ({
  userId,
  secret,
  refreshTokenVersion,
  expiresImmediately,
}: {
  userId: number;
  secret: string;
  refreshTokenVersion: number;
  expiresImmediately?: boolean;
}) => {
  const expiresIn = expiresImmediately ? '0s' : '7d';
  return sign({ userId, refreshTokenVersion }, secret, { expiresIn: expiresIn });
};

export const setRefreshTokenCookie = ({ refreshToken, reply }: { refreshToken: string; reply: FastifyReply }) => {
  reply.setCookie('refresh', refreshToken, { httpOnly: true, sameSite: 'none', secure: true });
};

export const verifyToken = <T extends TokenPayload>({ token, secret }: { token: string; secret: string }): T => {
  try {
    const payload = verify(token, secret);
    return payload as T;
  } catch (e) {
    throw new Error('Unauthenticated');
  }
};
