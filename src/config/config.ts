import { z } from "zod";
import * as dotenv from "dotenv";
import { join } from "path";

dotenv.config({ path: join(__dirname, "../../.env") });

const envSchema = z.object({
  PORT: z.string().min(1),
  PG_CONNECTION_STRING: z.string().min(1),
  CORS_ALLOW_ORIGIN: z.string().min(1),
  JWT_SECRET: z.string().min(1),
  REFRESH_TOKEN_SECRET: z.string().min(1),
});

export type EnvConfig = z.infer<typeof envSchema>;

export const config = envSchema.parse(process.env);
