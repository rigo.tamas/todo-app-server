import fastify, { FastifyInstance } from 'fastify';
import cors from '@fastify/cors';
import mercurius from 'mercurius';
import { EnvConfig } from '../config/config';
import { schema } from '../graphql/schema';
import { GraphqlContext, resolvers } from '../graphql/resolvers';

import type { FastifyCookieOptions } from '@fastify/cookie';
import cookie from '@fastify/cookie';
import mercuriusAuth from 'mercurius-auth';
import {
  createAccessToken,
  createRefreshToken,
  RefreshTokenPayload,
  setRefreshTokenCookie,
  verifyToken,
} from '../auth/auth';
import { PrismaClient } from '@prisma/client';

type AuthContext = { userId?: number };

export const fastifyServerFactory = async ({
  graphqlContext,
  config,
  prisma,
}: {
  graphqlContext: GraphqlContext;
  config: EnvConfig;
  prisma: PrismaClient;
}): Promise<FastifyInstance> => {
  const fastifyServer = fastify({
    logger: true,
  });
  await fastifyServer.register(cors, {
    origin: config.CORS_ALLOW_ORIGIN,
    credentials: true,
  });
  fastifyServer.register(mercurius, {
    schema,
    context: graphqlContext,
    resolvers,
    graphiql: 'graphiql',
  });
  fastifyServer.register(cookie, {
    secret: 'not-important',
    parseOptions: {},
  } as FastifyCookieOptions);

  fastifyServer.register(mercuriusAuth, {
    authContext(context): AuthContext {
      const authHeader = context.request.headers['authorization'];
      if (!authHeader) {
        return { userId: undefined };
      }
      const accessToken = authHeader.split('Bearer ')[1];
      const payload = verifyToken({ token: accessToken, secret: config.JWT_SECRET });
      return { userId: payload.userId };
    },
    async applyPolicy(authDirectiveAST, parent, args, context, info) {
      const userId = (context?.auth as AuthContext)?.userId;
      if (userId === undefined) {
        throw new Error('Unauthorized');
      }
      return true;
    },
    authDirective: 'auth',
  });

  fastifyServer.post('/refresh_token', async (request, reply) => {
    const { refresh: refreshToken } = request.cookies;
    if (!refreshToken) {
      reply.code(401);
      throw new Error('no refresh token found');
    }
    try {
      const { userId, refreshTokenVersion } = verifyToken<RefreshTokenPayload>({
        token: refreshToken,
        secret: config.REFRESH_TOKEN_SECRET,
      });
      const user = await prisma.user.findUniqueOrThrow({ where: { id: userId } });
      if (user.refreshTokenVersion !== refreshTokenVersion) {
        reply.code(401);
        throw new Error('Unauthenticated');
      }
      const jwt = createAccessToken({ userId, secret: config.JWT_SECRET });

      const newRefreshToken = createRefreshToken({
        userId,
        secret: config.REFRESH_TOKEN_SECRET,
        refreshTokenVersion: user.refreshTokenVersion,
      });
      setRefreshTokenCookie({ refreshToken: newRefreshToken, reply });
      return { accessToken: jwt };
    } catch (e) {
      reply.code(401);
      throw e;
    }
  });

  return fastifyServer;
};
