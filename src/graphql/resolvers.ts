import { Collection, PrismaClient } from '@prisma/client';
import { FastifyReply, FastifyRequest } from 'fastify';
import { IResolvers } from 'mercurius';
import { generateSaltedHash } from '../utils/utils';
import { EnvConfig } from '../config/config';
import { compare } from 'bcrypt';
import { createAccessToken, createRefreshToken, setRefreshTokenCookie } from '../auth/auth';

type PromiseType<T> = T extends PromiseLike<infer U> ? U : T;

export type UnknownObject = Record<string, unknown>;

interface AuthInterface {
  userId: number;
}

declare module 'mercurius' {
  interface MercuriusContext extends PromiseType<ReturnType<ReturnType<typeof graphqlContextFactory>>> {}
}
declare module 'mercurius-auth' {
  interface MercuriusAuthContext extends AuthInterface {}
}

const wait = (ms: number) => {
  return new Promise((res) => setTimeout(res, ms));
};

export type GraphqlContext = (req: FastifyRequest, _reply: FastifyReply) => Promise<UnknownObject>;

export const graphqlContextFactory = ({ config, prisma }: { config: EnvConfig; prisma: PrismaClient }) => {
  return async (request: FastifyRequest, reply: FastifyReply) => {
    return {
      config,
      prisma,
      request,
      reply,
    };
  };
};

export const resolvers: IResolvers = {
  Collection: {
    tasks: async (parent: Collection, args, ctx) => {
      if (!ctx.auth) {
        return;
      }
      return await ctx.prisma.task.findMany({
        where: { collectionId: parent.id, userId: ctx.auth.userId },
      });
    },
  },
  Task: {
    tags: async (parent: Collection, args, ctx) => {
      if (!ctx.auth) {
        return;
      }
      return await ctx.prisma.tag.findMany({
        where: { taskId: parent.id, userId: ctx.auth.userId },
      });
    },
  },

  Query: {
    collection: async (parent, args: { id: number }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      return await ctx.prisma.collection.findFirst({
        where: { id: args.id, userId: ctx.auth.userId },
      });
    },

    collections: async (parent, args: { id: number }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      return (await ctx.prisma.collection.findMany({ where: { userId: ctx.auth.userId } })) ?? [];
    },

    task: async (parent, args: { id: number }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      return await ctx.prisma.task.findFirst({
        where: { id: args.id, userId: ctx.auth.userId },
      });
    },
  },
  Mutation: {
    register: async (_parent, args: { name: string; password: string }, ctx) => {
      const isAlreadyExist = await ctx.prisma.user.findUnique({
        where: { name: args.name },
        select: { _count: true },
      });
      if (isAlreadyExist) {
        return { ok: false };
      }
      const passwordHash = await generateSaltedHash({
        password: args.password,
      });
      const user = await ctx.prisma.user.create({
        data: { passwordHash, name: args.name },
      });
      const jwt = createAccessToken({ userId: user.id, secret: ctx.config.JWT_SECRET });
      const refreshToken = createRefreshToken({
        userId: user.id,
        secret: ctx.config.REFRESH_TOKEN_SECRET,
        refreshTokenVersion: user.refreshTokenVersion,
      });
      setRefreshTokenCookie({ refreshToken, reply: ctx.reply });
      return { accessToken: jwt, ok: true };
    },

    login: async (_parent, args: { name: string; password: string }, ctx) => {
      const user = await ctx.prisma.user.findUniqueOrThrow({
        where: { name: args.name },
      });
      const valid = await compare(args.password, user.passwordHash);
      if (!valid) {
        return { ok: false };
      }
      const jwt = createAccessToken({ userId: user.id, secret: ctx.config.JWT_SECRET });
      const refreshToken = createRefreshToken({
        userId: user.id,
        secret: ctx.config.REFRESH_TOKEN_SECRET,
        refreshTokenVersion: user.refreshTokenVersion,
      });
      setRefreshTokenCookie({ refreshToken, reply: ctx.reply });
      return { accessToken: jwt, ok: true };
    },

    logout: async (_parent, args: { name: string; password: string }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      const refreshToken = createRefreshToken({
        userId: ctx.auth.userId,
        secret: ctx.config.REFRESH_TOKEN_SECRET,
        refreshTokenVersion: -1,
        expiresImmediately: true,
      });
      setRefreshTokenCookie({ refreshToken, reply: ctx.reply });
      return { ok: true };
    },

    createCollection: async (_parent, args: { name: string }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      return await ctx.prisma.collection.create({
        data: {
          name: args.name,
          userId: ctx.auth.userId,
        },
      });
    },

    createTask: async (_parent, args: { name: string; collectionId: number }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      return await ctx.prisma.task.create({
        data: {
          name: args.name,
          collectionId: args.collectionId,
          userId: ctx.auth.userId,
        },
      });
    },

    createTag: async (_parent, args: { name: string; taskId: number }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      return await ctx.prisma.tag.create({
        data: {
          name: args.name,
          taskId: args.taskId,
          userId: ctx.auth.userId,
        },
      });
    },

    updateTaskCompletion: async (_parent, args: { taskId: number; completed: boolean }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      await ctx.prisma.task.updateMany({
        where: { id: args.taskId, userId: ctx.auth.userId },
        data: { completed: args.completed },
      });
      return { completed: args.completed };
    },

    updateTaskName: async (_parent, args: { taskId: number; taskName: string }, ctx) => {
      if (!ctx.auth) {
        return;
      }
      await ctx.prisma.task.updateMany({
        where: { id: args.taskId, userId: ctx.auth.userId },
        data: { name: args.taskName },
      });
      return { name: args.taskName };
    },
  },
};
