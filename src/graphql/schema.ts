export const schema = `
  directive @auth on OBJECT | FIELD_DEFINITION

  type Query {
    collection(id: Int!): Collection @auth
    collections: [Collection!]! @auth
    task(id: Int!): Task @auth
  }

  type Mutation {
    createCollection(name: String!): Collection! @auth
    createTask(name: String!, collectionId: Int!): Task! @auth
    updateTaskCompletion(taskId: Int!, completed: Boolean!): UpdateTaskCompletionResponse! @auth
    updateTaskName(taskId: Int!, taskName: String!): UpdateTaskNameResponse! @auth
    register(name: String!, password: String!): LoginResponse!
    login(name: String!, password: String!): LoginResponse!
    logout: OkResponse! @auth
    createTag(name: String!, taskId: Int!): Tag! @auth
  }

  type Task {
    id: Int!
    name: String!
    completed: Boolean!
    tags: [Tag!]!
  }

  type UpdateTaskNameResponse {
    name: String!
  }

  type UpdateTaskCompletionResponse {
    completed: Boolean!
  }

  type Tag {
    id: Int!
    name: String!
  }

  type Collection {
    id: Int!
    name: String!
    tasks: [Task!]!
  }

  type LoginResponse {
    accessToken: String
    ok: Boolean!
  }

  type OkResponse {
    ok: Boolean!
  }
`;
