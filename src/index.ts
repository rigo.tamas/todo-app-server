import { app } from "./app";
import { config } from "./config/config";

const startApp = async (): Promise<void> => {
  await app({ config });
};

process.on("SIGTERM", () => process.exit());

void startApp();
