import bcrypt from "bcrypt";

export const generateSaltedHash = async ({
  password,
}: {
  password: string;
}): Promise<string> => {
  const saltRounds = 10;
  return await bcrypt.hash(password, saltRounds);
};
